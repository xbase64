xbase64 (3.1.2-14) unstable; urgency=medium

  * Migrate to debhelper-compat 13:
    - debian/control: Add debhelper-compat (= 13).
    - Remove debian/compat.
    - debian/not-installed:
      + Add usr/bin/xbase64-config.
      + Add usr/lib/${DEB_HOST_MULTIARCH}/libxbase64.la.
  * Declare compliance with Debian Policy 4.6.1.0 (No changes needed).
  * debian/copyright:
    - Add year 2022 to myself.
  * Disable Link time optimization (Closes: #1015707):
    - debian/rules: Add optimize=-lto to DEB_BUILD_MAINT_OPTIONS.
  * debian/control: Add Rules-Requires-Root: no.

 -- Jörg Frings-Fürst <debian@jff.email>  Thu, 28 Jul 2022 07:45:46 +0200

xbase64 (3.1.2-13) unstable; urgency=medium

  * Change to my new mail address:
    - debian/control,
    - debian/copyright
  * Migrate to debhelper 11:
    - Change debian/compat to 11.
    - Bump minimum debhelper version in debian/control to >= 11.
    - Remove obsolate dh_autoreconf.
  * Declare compliance with Debian Policy 4.2.1 (No changes needed).
  * Add new README.source to explain the branching model used.
  * New debian/source/lintian-overrides.
  * debian/patches/0120-fix-types-include.diff:
    - Fix typo.
  * debian/control:
    - Switch Vcs-* to new location.
  * debian/copyright:
    - Use secure copyright format URI.
    - Change years for me.
  * debian/watch:
    - Use secure URI.

 -- Jörg Frings-Fürst <debian@jff.email>  Wed, 03 Oct 2018 12:27:12 +0200

xbase64 (3.1.2-12) unstable; urgency=medium

  * Rewrite *.maintscript for unhandled DPKG_MAINTSCRIPT_PACKAGE
    (Closes: #843467).
    - Thanks to Andreas Beckmann <anbe@debian.org>

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Mon, 13 Feb 2017 17:13:35 +0100

xbase64 (3.1.2-11) unstable; urgency=medium

  * Remove *.maintscript symlinks and use DPKG_MAINTSCRIPT_PACKAGE
    to determine the package name in maintscript (Closes: #843467).

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Fri, 09 Dec 2016 09:59:53 +0100

xbase64 (3.1.2-10) unstable; urgency=medium

  * Fix unhandled symlink to directory conversion (Closes: #843467):
    - debian/control:
      + Add Pre-Depends: dpkg (>= 1.17.5) to libxbase64-doc.
      + Add Pre-Depends: ${misc:Pre-Depends} to libxbase64-bin, libxbase64-dev
        and libxbase64-1.
    - Replace *.postinst with *.maintscript.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Wed, 09 Nov 2016 04:14:39 +0100

xbase64 (3.1.2-9) unstable; urgency=medium

  * New debian/patches/0135-replace_gets.patch:
    - Replace deprecated gets() with std::cin.getline() (Closes: #841626).

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sat, 22 Oct 2016 12:47:33 +0200

xbase64 (3.1.2-8) unstable; urgency=medium

  * Add symbols file for powerpcspe to fix FTBFS.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Mon, 03 Oct 2016 13:41:56 +0200

xbase64 (3.1.2-7) unstable; urgency=medium

   * debian/patches:
     - Add patch to fix incorrect use of std::cout (Closes: #811846).
       Thanks to Pino Toscano <pino@debian.org>.
     - Renumber patches.
   * debian/control:
     - Change Vcs-* to secure URI.
     - Bump Standards-Version to 3.9.8 (no changes required).
     - Change debhelper version build dependency to >= 10.
     - Add missing libxbase64-doc to Depends for libxbase64-[1|bin|dev].
   * debian/copyright:
     - Add year 2016 for debian/*.
   * debian/rules:
     - Simplify override_dh_auto_configure.
     - Remove override_dh_installdocs to fix FTBFS.
   * debian/watch:
     - Bump version to 4 (no changes required).
   * Bump compat level to 10.
   * Use dpkg-maintscript-helper dir_to_symlink in debian/*.postinst.
   * Remove useless debian/libxbase64-doc.lintian-overrides.
   * Remove useless debian(libxbase64-1.dirs.
   * Add symbols files for all architectures.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Tue, 20 Sep 2016 06:00:07 +0200

xbase64 (3.1.2-6) unstable; urgency=low

  * debian/rules:
    - Split override_dh_install into *-arch and *-indep (Closes: #806123).

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Sat, 12 Dec 2015 16:13:27 +0100

xbase64 (3.1.2-5) unstable; urgency=low

  * debian/rules:
    - Rename arch-dependent file xbconfig.h to xbconfig-[MULTIARCH].h
      (Closes: #762233).
    - Remove useless hardening parts.
  * Add year 2015 to debian/copyright.
  * Remove debian/source/options because xz compression is standard now.
  * debian/control:
    - Bump Standards-Version to 3.9.6 (no changes required).
    - Change Vcs-Browser to cgit.
    - Remove Multi-Arch: same from libxbase64-dev.
    - Rewrite Depends.
  * New debian/patches/0500-ReproducibleBuilds.patch:
    - Remove timestamps from doxygen generated files.
  * Renumber debian/patches/*.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Tue, 17 Feb 2015 14:15:14 +0100

xbase64 (3.1.2-4) unstable; urgency=low

  * New maintainer (Closes: #756543).
  * debian/copyright:
    - Rewrite in machine-readable format.
    - Add myself to the list of authors for debian/*.
  * Change debian/compat to 9 (no changes required).
  * Add debian/source/options:
    - Set compression to xz and compression-level to 9 to save space.
  * debian/control:
    - Set myself as maintainer.
    - Update Build-Depends:
      + Change debhelper version to (>= 9).
    - Remove duplicate Section entries in binary control fields.
    - For previously not existing Vcs:
      + Create a new git repository on alioth.
      + Add the fields Vcs-Browser and Vcs-Git.
    - Move HTML documentation from package libxbase64-dev to libxbase64-doc.
    - Add multiarch support:
      + libxbase64-dev.
      + libxbase64-1.
  * debian/rules:
    - Enable hardening.
    - Rewrite for modern dh.
  * New debian/*.symbols:
    - Add symbol files.
  * New debian/*.manpages:
    - Add to install the manpages.
  * New debian/man/*.1:
    - Add manpages for binary files.
  * New debian/libxbase64-doc.lintian-overrides:
    - Add duplicate-files for some doxygen PNGs.
  * New debian/patches/0100-typo.patch:
    - Add some fixes for typos.
  * debian/patches/fixconfig.diff
    - Add fix for typo.
  * debian/patches/*
    - Add DEP-3 header.
  * Merge QA upload 3.1.2-3 into my work.
  * Add doc directory as symlink from libxbase64-doc:
    - Add *.postinst to save changes in the old doc dir.

 -- Jörg Frings-Fürst <debian@jff-webhosting.net>  Mon, 15 Sep 2014 15:36:21 +0200

xbase64 (3.1.2-3) unstable; urgency=medium

  * QA upload.
  * Extend lesserg.diff to cover also an occurrency of "__GNU LesserC__".
  * Fix xbtypes.h so it can be included more than one time without issues,
    and make sure it is not included at the wrong time;
    patch fix-types-include.diff.

 -- Pino Toscano <pino@debian.org>  Sat, 13 Sep 2014 11:11:18 +0200

xbase64 (3.1.2-2) unstable; urgency=medium

  * QA upload.
  * Mention GPL license for various binaries and scripts (Closes: #746178).
  * libxbase64-bin: Conflict with dvb-apps, both providing a `zap' binary.
    Assuming that Linux TV and Xbase utilities don't need to be installed
    together (Closes: #746215).

 -- Matthias Klose <doko@debian.org>  Mon, 28 Apr 2014 11:42:01 +0200

xbase64 (3.1.2-1) unstable; urgency=medium

  * QA upload. Initial upload, replacing libxbase (Closes: #743443).
    Package xbase64 is only seven years old instead of eleven like 2.x.
    Make sure that calligra still builds with the updated version.
  * Bump standards version to 3.9.5.
  * Clean up overall packaging.

 -- Matthias Klose <doko@debian.org>  Wed, 02 Apr 2014 20:33:01 +0200

libxbase (2.0.0-8.5) unstable; urgency=low

  * Non-maintainer upload.
  * Fix build failure with GCC 4.7 (Closes: #667260).

 -- Matthias Klose <doko@debian.org>  Mon, 16 Apr 2012 23:00:27 +0200

libxbase (2.0.0-8.4) unstable; urgency=low

  * Non-maintainer upload.
  * Build with current automake (Closes: #549134).

 -- Moritz Muehlenhoff <jmm@debian.org>  Mon, 30 May 2011 05:12:34 +0200

libxbase (2.0.0-8.3) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS on GNU/kFreeBSD by handling kfreebsd* the same way as
    linux-gnu* in ltconfig, thanks to Petr Salinger (Closes: #493509).
  * Also add ${shlibs:Depends} to libxbase2.0-bin's Depends, a libc
    dependency was missing for some binaries in there.
  * Finally call dh_installdeb after dh_shlibdeps as per lintian warning.

 -- Cyril Brulebois <kibi@debian.org>  Thu, 20 Aug 2009 01:58:55 +0200

libxbase (2.0.0-8.2) unstable; urgency=low

  * Non-maintainer upload.
  * Move include files to /usr/include (Closes: #470260).

 -- Barry deFreese <bddebian@comcast.net>  Mon, 10 Mar 2008 15:15:06 -0400

libxbase (2.0.0-8.1) unstable; urgency=low

  * Non-maintainer upload.
  * Add watch file.
  * Add Homepage field to control.
  * Add Section to source package and -dev package to libdevel.
  * Fix up debian/copyright syntax.
  * Call dh_makeshlibs before dh_shlibdeps (Closes: #453792).
  * Make clean not ignore errors.
  * Version debhelper build dependency.
  * Move DH_COMPAT to debian/compat and set to 5.
  * Remove unneeded dirs file.
  * Replace 'pwd' with $(CURDIR).
  * Move .files files to .install and adjust debian/rules accordingly.
  * Build with gcc-4.3 (Closes: #461714).
    - Include <iostream> instead of iostream.h in xbase/xbase.h
      to fix FTBFS of reverse dependencies (Closes: #299114).
  * Make binNMU-safe. Replace Source-Version with binary:Version.
  * Handle nostrip option (Closes: #437457).
  * Add info to description that docs are in -dev package (Closes: #394259).
  * Bump Standards Version to 3.7.3.

 -- Barry deFreese <bddebian@comcast.net>  Thu, 21 Feb 2008 11:04:08 -0500

libxbase (2.0.0-8) unstable; urgency=low

  * Remove "#pragma interface" to make it build with g++ 4.1 (Closes: #356245).
    Thanks to Ben Hutchings and Brian M. Carlson!

 -- Michael Vogt <mvo@debian.org>  Fri, 12 May 2006 11:16:43 +0200

libxbase (2.0.0-7.1) unstable; urgency=low

  * Non-maintainer upload for C++ transition.
  * debian/control:
    - Rename libxbase2.0-0c102 to libxbase2.0-0.

 -- Jeroen van Wolffelaar <jeroen@wolffelaar.nl>  Sat,  6 Aug 2005 02:54:09 +0200

libxbase (2.0.0-7) unstable; urgency=low

  * Add patch from Wesley J Landake to remove xbNtx:: scope for
    AllocKeyBufs xbase/ntx.h (Closes: #253655). Thanks to Wesley J Landake!
  * Add missing long description for binary packages (Closes: #209746).
    Thanks to Wesley J Landake!

 -- Michael Vogt <mvo@debian.org>  Tue, 14 Sep 2004 10:31:34 +0200

libxbase (2.0.0-6) unstable; urgency=low

  * Rebuild for g++-3.2.

 -- Michael Vogt <mvo@debian.org>  Sun, 12 Jan 2003 15:06:50 +0100

libxbase (2.0.0-5) unstable; urgency=low

  * Split libxbase into libxbase2.0-dev and libxbase2.0-bin (Closes: #148308).
  * Fix a bug that prevented the examples from being installed.

 -- Michael Vogt <mvo@debian.org>  Wed, 29 May 2002 11:11:23 +0200

libxbase (2.0.0-4) unstable; urgency=low

  * Rename binary lib packages to match soname (Closes: #141757).
  * Add doxygen to Build-Depends (Closes: #141947).

 -- Michael Vogt <mvo@debian.org>  Tue,  9 Apr 2002 09:04:15 +0200

libxbase (2.0.0-3) unstable; urgency=low

  * Update config.sub, config.guess (automated via autotools-dev).

 -- Michael Vogt <mvo@debian.org>  Mon,  8 Apr 2002 09:31:05 +0200

libxbase (2.0.0-2) unstable; urgency=low

  * debian/control:
    - Rename binary package xbase to libxbase to prevent name clash
      with the old X11 package "xbase" (Closes: #141686).

 -- Michael Vogt <mvo@debian.org>  Mon,  8 Apr 2002 09:02:35 +0200

xbase (2.0.0-1) unstable; urgency=low

  * New upstream release.

 -- Michael Vogt <mvo@debian.org>  Sun, 17 Mar 2002 01:00:42 +0100

xdb (1.2.0-4) unstable; urgency=low

  * Update config.guess and config.sub to fix FTBFS on hppa (Closes: #96607).

 -- Michael Vogt <mvo@debian.org>  Mon,  7 May 2001 10:38:44 +0200

xdb (1.2.0-3) unstable; urgency=low

  * Update config.guess and config.sub to fix FTBFS on arm (Closes: #90222).

 -- Michael Vogt <mvo@debian.org>  Mon, 16 Apr 2001 21:54:31 +0200

xdb (1.2.0-2) unstable; urgency=low

  * New maintainer (Closes: #88240).
  * Add Build-Depends field to debian/control.

 -- Michael Vogt <mvo@debian.org>  Thu, 12 Apr 2001 12:09:39 +0200

xdb (1.2.0-1) unstable; urgency=low

  * Initial release.

 -- Ionutz Borcoman <borco@borco-ei.eng.hokudai.ac.jp>  Thu, 10 Aug 2000 12:08:53 +0900
